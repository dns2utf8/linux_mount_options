//use std::path::{Path, PathBuf};

fn main() -> std::io::Result<()> {
    let mut args = std::env::args();

    // skip binary name
    args.next();

    for arg in args {
        let info = linux_mount_options::detect(&arg)?;
        println!("{}: {:?}", arg, info);
    }

    Ok(())
}
