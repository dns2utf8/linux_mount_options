# linux_mount_options

[![crates.io](https://img.shields.io/crates/v/linux_mount_options?logo=rust)](https://crates.io/crates/linux_mount_options/)
[![docs.rs](https://docs.rs/linux_mount_options/badge.svg)](https://docs.rs/linux_mount_options)

Detect file system, mount point and noatime of a folder the process has access to.

## Requirements

* Access to `/proc/mounts`
* Linux tested
    * It should work on *BSD. However, it is not tested.

# Example:

First, add it to your `Cargo.toml`:

```yaml
[dependencies]
linux_mount_options = "0"
```

In your `main.rs`:

```rust
fn main() -> Result<(), Box<dyn Error>> {
    let path = "/home";
    let info = linux_mount_options::detect(&path)?;
    println!("{}: {:?}", path, info);
    Ok( () )
}
```

If you are using logging based on the [log infrastructure](https://crates.io/crates/log) you will get timestamped and formatted output.
